DROP TABLE IF EXISTS EMPLOYEE;
DROP TABLE IF EXISTS COMPANY;
CREATE TABLE `company`(
                          ID bigint auto_increment not null,
                          name VARCHAR(255),
                          primary key (ID)
);

CREATE TABLE `employee`(
    ID bigint auto_increment not null,
    name VARCHAR(255),
    age int,
    gender varchar(255),
    salary double,
    company_id bigint,
    primary key (ID),
    foreign key (company_id) references company(ID)
);

